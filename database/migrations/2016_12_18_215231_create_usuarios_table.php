<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario');
            $table->string('correo')->unique();
            $table->integer('cat_tipo_usuario_id')->unsigned();
            $table->integer('asociado_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('cat_tipo_usuario_id')->references('id')->on('cat_tipo_usuarios');
            $table->foreign('asociado_id')->references('id')->on('asociados');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('usuarios');
    }
}
