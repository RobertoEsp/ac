<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAcuerdosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('acuerdos', function (Blueprint $table) {

            $table->increments('id');

            $table->string('acuerdo');//cual es el acuerdo
            $table->integer('mocion')->unsigned();//quien hace la moción
            $table->integer('secundo')->unsigned();//quien secundó

            $table->date('creacion');//cuando fue acordado
            $table->string('sesion');//en que sesion fue acordado

            $table->timestamps();
            $table->softDeletes();


            $table->foreign('mocion')->references('id')->on('asociados');
            $table->foreign('secundo')->references('id')->on('asociados');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('acuerdos');
    }
}
